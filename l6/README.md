# Lab 6

## k8s start.

```bash
cd terraform
terraform init
terraform plan
terraform apply -auto-approve
```

## apps

```bash
docker login
```

```bash
cd apps/search_engine_crawler
docker build -t labdima4/crawler:lab6 .
docker push labdima4/crawler:lab6
```

```bash
cd apps/search_engine_ui
docker build -t labdima4/ui:lab6 .
docker push labdima4/ui:lab6
```

```bash
gcloud init
gcloud auth application-default login
```

## k8s credits
```bash
gcloud container clusters get-credentials gke-cluster --region europe-west3
```

```bash
kubectl apply -f ./charts/dev-namespace.yml
```

## helm

```bash
helm repo add stable https://charts.helm.sh/stable --force-update
```

### base command 
```bash
helm search mongodb
helm install chart-path --name release name
helm upgrade chart-path chart-dir/
helm ls
```

### install apps
```bash
helm dep update ./search-engine
helm install ./search-engine --generate-name
 NAME: search-engine-1609696370
 LAST DEPLOYED: Sun Jan  3 19:52:51 2021
 NAMESPACE: default
 STATUS: deployed
 REVISION: 1
 TEST SUITE: None
```
### uninstall all failed
```bash
helm ls | grep fail | awk '{print $1}' | xargs helm uninstall
```

### App testing:
+ Check up and running search-engine-app on https://ingress_IP/

### destroy

```bash
helm uninstall search-engine-1609696370
```

```bash
terraform destroy -auto-approve
```