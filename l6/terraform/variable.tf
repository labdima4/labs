variable "project" {
  description = "The project ID to host the cluster in"
}

variable "cluster_name" {
  description = "A suffix to append to the default cluster name"
  default     = "gke-cluster"
}

variable "region" {
  description = "The region to host the cluster in"
}

variable "network_name" {
  description = "The VPC network to host the cluster in"
  default     = "gke-network"
}
variable "subnetwork" {
  description = "The subnetwork created to host the cluster in"
  default     = "gke-subnet"
}

variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}

variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-scv"
}

variable "skip_provisioners" {
  type        = bool
  description = "Flag to skip local-exec provisioners"
  default     = false
}

variable "service_account" {
  description = "Google service account"
}
