# Lab 5

## k8s start. 

```bash
cd terraform
terraform init
terraform plan
terraform apply -auto-approve
```

## apps

```bash
docker login
```

```bash
cd apps/search_engine_crawler
docker build -t labdima4/crawler:lab5 .
docker push labdima4/crawler:lab5
```

```bash
cd apps/search_engine_ui
docker build -t labdima4/ui:lab5 .
docker push labdima4/ui:lab5
```

```bash
gcloud init
gcloud auth application-default login
```

## k8s credits
```bash
gcloud container clusters get-credentials gke-cluster --region europe-west3
```

## k8s deploy apps
```bash
cd l5/
```

```bash
kubectl apply -f ./deploy/dev-namespace.yml
```

```bash
kubectl apply -f ./deploy/ -n dev
```

### check failed pod
```bash
kubectl get pods
kubectl get pod failed-pod-name --output=yaml
```

### get ingress ip
```bash
kubectl get ingress -n dev
```
```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=ingress_IP"
```
```bash
kubectl create secret tls ui-ingress --key tls.key --cert tls.crt -n dev
```
### App testing:
+ Check up and running search-engine-app on https://ingress_IP/
+ For preparing ```my-secret.yml``` manifest check [Kubernetes Secrets](https://kubernetes.io/docs/concepts/configuration/secret/)

### clear dev namespace
```bash
kubectl delete -f deploy/dev-namespace.yml
```

### k8s shutdown
```bash
cd terrafrom
terraform destroy -auto-approve
```
