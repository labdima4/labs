from os import getenv
from pika import BlockingConnection, ConnectionParameters, PlainCredentials

mqqueue = getenv('RMQ_QUEUE', 'aaa')


def callback(ch, method, properties, body):
    print(method_frame.delivery_tag)
    print(body)
    print()
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    return


if __name__ == "__main__":
    try:
        mqhost = "localhost"
        mqport = 5672
        credentials = PlainCredentials("guest", "guest")
        rabbit = BlockingConnection(ConnectionParameters(
            host=mqhost,
            port=mqport,
            virtual_host="/",
            connection_attempts=10,
            retry_delay=1,
            credentials=credentials))
    except Exception as e:
        print("connect_to_MQ crawler Failed connect to MQ")
        print(e)
    else:
        print('connect_to_MQ crawler - Successfully connected to MQ host {}'.format(mqhost))

    channel = rabbit.channel()
    channel.queue_declare(queue=mqqueue)
    channel.basic_consume(callback, queue=mqqueue)
    channel.start_consuming()
