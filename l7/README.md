# Lab 7

## apps

```bash
docker login
```

```bash
cd fluentd
docker build -t labdima4/fluentd . 
docker push labdima4/fluentd
```

```bash
cd search_engine_crawler
docker build -t labdima4/crawler:lab7 .
docker push labdima4/crawler:lab7
```

```bash
cd search_engine_ui
docker build -t labdima4/ui:lab7 .
docker push labdima4/ui:lab7
```

## docker compose

```bash
sysctl -w vm.max_map_count=262144
docker-compose -f docker-logging.yml up -d
```

```bash
docker-compose up -d
```

## clear

```bash
docker-compose -f docker-logging.yml down --rmi all -v
```

```bash
docker-compose down --rmi all -v
```
