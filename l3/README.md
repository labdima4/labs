# Lab3

```bash
nomad agent -dev
```

```bash
nomad run lab3.nomad
```

```bash
docker ps

CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                                                    NAMES
cb6eaab866e8        mongo                 "docker-entrypoint.s…"   2 seconds ago       Up 1 second         127.0.0.1:23726->27017/tcp, 127.0.0.1:23726->27017/udp   mongodb-670eba52-bec1-f10c-5660-05e075cdf637
42751e7fa437        hashicorp/http-echo   "/http-echo -text 'h…"   3 seconds ago       Up 1 second         127.0.0.1:22593->5678/tcp, 127.0.0.1:22593->5678/udp     server-55fe02a0-8a80-5c11-3c3a-4247ba7d2c42
13a7f9063d60        hashicorp/http-echo   "/http-echo -text 'h…"   3 seconds ago       Up 1 second         127.0.0.1:26118->5678/tcp, 127.0.0.1:26118->5678/udp     server-c511132e-5e69-3f0a-3a39-e93f65880b7e
67a2f481b33b        hashicorp/http-echo   "/http-echo -text 'h…"   3 seconds ago       Up 1 second         127.0.0.1:21772->5678/tcp, 127.0.0.1:21772->5678/udp     server-0afb3b56-9eec-0cd8-968d-ffa4f8d22181
```
```bash
curl 127.0.0.1:26118
hello world
```

```bash
nomad ui
```

```bash
nomad job stop lab3
```
