# Lab 8

## apps

```bash
docker login
```

```bash
cd alertmanager
docker build -t labdima4/alertmanager . && docker push labdima4/alertmanager
```

```bash
cd cloudprober
docker build -t labdima4/cloudprober . && docker push labdima4/cloudprober
```

```bash
cd grafana
docker build -t labdima4/grafana . && docker push labdima4/grafana
```

```bash
cd prometheus
docker build -t labdima4/prometheus . && docker push labdima4/prometheus
```
```bash
cd search_engine_crawler
docker build -t labdima4/crawler:lab8 . && docker push labdima4/crawler:lab8
```

```bash
cd search_engine_ui
docker build -t labdima4/ui:lab8 . && docker push labdima4/ui:lab8
```

## docker compose

```bash
docker-compose -f docker-monitoring.yml up -d
```

```bash
docker-compose up -d
```

## clear

```bash
docker-compose -f docker-monitoring.yml down --rmi all -v
```

```bash
docker-compose down --rmi all -v
```
