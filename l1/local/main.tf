resource "local_file" "foo" {
  count    = var.file_count
  content  = <<-EOT
    file #${count.index}
  EOT
  filename = "./files/${var.file_name}-${count.index}"
}
