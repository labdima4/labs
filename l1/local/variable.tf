variable file_count {
  type = number
  description = "Count files"
  default = 1
}

variable file_name {
  description = "File Name"
  default = "file"
}
