output "app_external_ip" {
  value = "${google_compute_instance.app.*.network_interface.0.access_config.0.nat_ip}"
  description = "Public IPv4 address of the created above instance"
}
