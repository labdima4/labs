provider "google" {
  project = var.project
  region = var.region
  zone   = var.zone
}

resource "google_compute_instance" "app" {
  name         = "lab1-app-${count.index}"
  machine_type = var.mach_type
  zone         = var.zone
  count        = "3"

  tags = ["lab1", "foo"]

  boot_disk {
    initialize_params {
      image = var.app_disk_image
    }
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  metadata = {
    # путь до публичного ключа
    ssh-keys = "appuser:${file(var.public_key_path)}"
  }
}


resource "google_compute_firewall" "firewall_lab1" {
  name = "allow-labs-default"
  network = "default"
  allow {
    protocol = "tcp"
    ports = ["80", "443"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["lab1"]
}
