variable name {
  description = "Project"
}

variable project {
  description = "Project"
}

variable region {
  description = "Region"
}

variable zone {
  description = "Zone"
}

variable private_key_path {
  description = "Path to the public key used to connect to instance"
}

variable public_key_path {
  description = "Path to the public key used to connect to instance"
}

variable app_disk_image {
  description = "Disk image for app"
}

variable mach_type {
  description = "Machine type"
  default = "f1-micro"
}
