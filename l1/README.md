#Lab #1

```bash
ssh-keygen
```

##gcp

```bash
gcloud auth login
```

```bash
gcloud config set project GCP_PROJECT_NAME
```

```bash
gcloud compute images list
```

```
veriable.tf
terraform.tfvars
disk_image       = "ubuntu-minimal-2004-focal-v20201211"
```

```bash
terraform init
``` 

```bash
terraform plan
``` 

```bash
terraform apply -auto-approve
```

```bash
terraform destroy -auto-approve
```

## local-file

```bash
terraform init
``` 

```bash
terraform plan
``` 

```
veriable.tf

terraform.tfvars

file_name  = "filename"
file_count = 3
```

```bash
terraform apply -auto-approve
```

```bash
terraform destroy -auto-approve
```
