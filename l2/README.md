# Lab2

```bash
sudo snap install docker
sudo apt install docker-compose
```
```bash
sudo chmod 666 /var/run/docker.sock
```
or
```bash
sudo usermod -a -G docker ${USER}
```

```bash
gcloud auth login
```

```bash
gcloud config set project GCP_PROJECT_NAME
```

Enable gcr.io registry at console.cloud.google.com 

```bash
gcloud iam service-accounts keys create ~/key.json \
  --iam-account sa-name@project-id.iam.gserviceaccount.com
```
```bash
gcloud auth activate-service-account sa-name@project-id.iam.gserviceaccount.com \
 --key-file=~/key.json
```

```bash
gcloud auth configure-docker
```

```bash
docker ps
docker images
docker images -a
docker system prune -a
```

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
```
```bash
cat > ~/.profile
export NVM_DIR="$HOME/.nvm"gcloud iam service-accounts keys create ~/key.json \
  --iam-account sa-name@project-id.iam.gserviceaccount.com
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
. ~/.profile
```

```bash
nvm install 12
```

## docker-build-push-gcr

```bash
npm install
```

```bash
pulumi stack init dev
```

```bash
pulumi config set gcp:project <projectname> 
pulumi config set gcp:region <region>
```

```bash
pulumi up
```

## cloud-run-deploy

```bash
npm install
```

```bash
pulumi stack init dev
```

```bash
pulumi config set gcp:project <projectname> 
pulumi config set gcp:region <region>
pulumi config set docker-config-file ~/.docker/config.json
```

```bash
pulumi up
```

```bash
curl "$(pulumi stack output rubyUrl)"
Hello Pulumi!
```

```bash
pulumi destroy
...
pulumi stack rm dev
...
```
